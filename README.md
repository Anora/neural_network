# Project setup

## Python 3.6+ x64 & Win x64 required

## Setup a virtual environment
```
virtualenv venv
cd venv/Scripts
activate
```

## Install requirements.
```
pip install -r requirements.txt
```

run train.py to create and train neural network and then run test.py to test nn on distinct images.