from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense
from keras.utils import np_utils


# Берем данные из mnist датасета
(X_train, y_train), (X_test, y_test) = mnist.load_data()

# Меняем размерность
X_train = X_train.reshape(60000, 784)
X_test = X_test.reshape(10000, 784)

# Нормализуем
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')

X_train /= 255
X_test /= 255

# Преобразуем метки в one-hot encoding
Y_train = np_utils.to_categorical(y_train, 10)
Y_test = np_utils.to_categorical(y_test, 10)

# Создаем модель
model = Sequential()
model.add(Dense(800, input_dim=784, activation="relu"))
model.add(Dense(10, activation="softmax"))

# Компилируем модель
model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

# Тренируем
model.fit(X_train, Y_train, batch_size=200, epochs=20,  verbose=1)

# Сохраняем модель и ее веса
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)

model.save_weights("model.h5")
print("Saved model to disk")
