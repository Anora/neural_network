import numpy as np
import matplotlib.pyplot as plt
from keras.models import model_from_json
from keras.preprocessing import image


# Открываем готовую модель и загружаем веса
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights("model.h5")
print("Loaded model from disk")

# читываем название картинки на проверку
img_path = input('Enter name of image: ')
img = image.load_img(img_path, target_size=(28, 28), color_mode = "grayscale")
# Конвертируем и отображаем на экране
plt.imshow(img.convert('RGBA'))
plt.show()

# риводим изображение к нужному виду
x = image.img_to_array(img)
x = x.reshape(1, 784)
x = 255 - x
x /= 255

# Проверяем изображение моделью
prediction = loaded_model.predict(x)
# Выводим верный ответ
print('Вероятно, это цифра', np.argmax(prediction))
